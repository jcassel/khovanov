diagram <- tf


## Helper functions
calculate_intersection_basis <- function(b1,b2){
  m <- cbind(b1 , - b2)
  m <- NullSpace(m)
  m <- b1%*%m[1:ncol(b1),]
  return(m)
}

clean_up_H <- function(H){
  ## clean out the empty cells
  i <- 1
  while(TRUE){
    if(class(H[[i]]) != "homology"){
      H[[i]] <- NULL
    } else i <- i+1
    if(i > length(H)) break
  }


  ## add up the correct bases and delete doublings
  v <- numeric(0)
  for(i in 1:(length(H)-1) ){
    degree <- H[[i]]$degree
    if(H[[i+1]]$degree == degree){
      H[[i]]$basis <- direct.sum(H[[i]]$basis,H[[i+1]]$basis)

      H[[i]]$cbs <- complete_basis(H[[i]]$basis)

      if(all(H[[i]]$basis == 0)) H[[i]]$rel <- integer(0)
      else H[[i]]$rel <- which(apply(basis,2,function(x) all(x == 0)))

      H[[i]]$spcs <- c(H[[i]]$spcs,H[[i+1]]$spcs)

      i <- i+1
      v <- c(v,i)
    }
  }
  for(x in v) H[[x]] <- NULL

  ## shift the grading
  for(x in 1:length(H)) H[[x]]$degree <- H[[x]]$degree+1

  return(H)
}

replace_by_0 <- function(){
  if(all(curr == 0)) curr <<- 0
}

is_inside <- function(y,x){
  # cat(" is in " ," ?: ")
  R(cbind(y,x)) == R(y)
}

complete_basis <- function(mtrx){
  if(all(mtrx == 0)) return(diag(1 , nrow = nrow(mtrx)))

  dim <- nrow(mtrx)
  q <- diag(1 , nrow = dim)
  basis <- get_homology_basis(mtrx,q,rk1 = qr(mtrx)$rank,rk2 = dim)

  return(cbind(mtrx,basis))
}

new_homology <- function(subspace,space,relation_space,space_indices,degree){
  structure(
    list(
      subspace = subspace,
      space = space,
      relation_space = relation_space,
      space_indices = space_indices,
      degree = degree
    ),
    class = "homology"
  )
}

homology <- function(mtrx,space,cycles,degree,relation_space){
  subspace <- mtrx
  # space <- complete_basis(subspace)

  space_indices <- list(cycles$space)

  return( new_homology(subspace,space,relation_space,space_indices,degree) )
}

# vector <- im_curr[,1]
# basis <- basis2
give_representor <- function(vector,basis,relation_space){
  if(is.null(relation_space)) return(vector)
  vector <- solve(basis,vector)
  vector[relation_space] <- 0
  return(basis%*%vector)
}


## compute the cone map between two cubes faces
index_vec1 <- c(0)
index_vec2 <- c(1)
level <- 1
get_cone_map <- function(diagram,level,index_vec1,index_vec2){
	crossings <- nrow(diagram)-level
	ls <- rep(as.list(0),crossings)
	ls <- lapply(ls, function(x) x <- c(0,1))
	grid <- expand.grid(ls)
	grid <- as_tibble(grid)

	m1 <- matrix(rep(index_vec1,each = nrow(grid)),nrow=nrow(grid))
	m2 <- matrix(rep(index_vec2,each = nrow(grid)),nrow=nrow(grid))


	grid0 <- cbind(grid, m1)
	grid1 <- cbind(grid, m2)

	grid0 %>% mutate(sum = rowSums(grid)) %>%
	  arrange(sum) %>% rowwise() -> grid0
	ls <- group_map(grid0,extract_cycles,diagram)
	grid0$list <- ls
	grid0 %>% ungroup() %>%
	  group_by(sum) -> grid0
	list0 <- group_map(grid0,get_input_lists)

	grid1 %>% mutate(sum = rowSums(grid)) %>%
	  arrange(sum) %>% rowwise() -> grid1
	ls <- group_map(grid1,extract_cycles,diagram)
	grid1$list <- ls
	grid1 %>% ungroup() %>%
	  group_by(sum) -> grid1
	list1 <- group_map(grid1,get_input_lists)

	f <- as.list(1:nrow(grid0))

	for(i in 1:nrow(grid0)){
	  f[[i]] <- get_map(grid0$list[i][[1]],grid1$list[i][[1]])
	}

	g <- as.list(0:max(grid0$sum))
	j <- 1
	for(i in 1:length(g) ){
	  g[[i]] <- f[[j]]$m
	  if(j < nrow(grid0)){
	    if(grid0$sum[j] == grid0$sum[j+1]){
	      g[[i]] <- direct.sum(g[[i]],f[[j+1]]$m)
	      j <- j+1
	     }
	  j <- j+1
	  }
	}

	return(g)
}
# get_cone_map(diagram,2,c(0,0),c(1,0))

# This map calculates the homology  V(*xy).
index_vec <- c(0,0)
baseline_homology <- function(diagram,index_vec){

	s0 <- c(0,index_vec)
	s1 <- c(1,index_vec)

	cycles1 <- get_cycles(diagram,s0)
	cycles2 <- get_cycles(diagram,s1)

	aux_f <- get_map(cycles1,cycles2)

  space <- diag(1,nrow=2^length(aux_f$s1))

	hom0 <- homology(NullSpace(aux_f$m),space,cycles1,0,NULL)

	q <- diag(1,nrow = 2^length(aux_f$s2))

  mtrx <- get_homology_basis(aux_f$m,q,rk1 = qr(aux_f$m)$rank,rk2 = qr(q)$rank)

  if(all(mtrx == 0)){
    subspace <- q
    relation_space <- 1:ncol(q)
    # mtrx <- matrix(0, nrow = nrow(aux_f$m), ncol = 1)
  } else {
    subspace <- cbind(mtrx,aux_f$m)
    relation_space <- (ncol(mtrx)+1):ncol(subspace)
  }



  hom1 <- homology(subspace,subspace,cycles2,1,relation_space)

	H <- list(hom0,hom1)
	return(H)
}
# H <- baseline_homology(diagram,c(0,0))

##### Compute the homology of a cone
# hom1: list of homology groups of space A
# hom2: list of homology groups of space B
# f: list of functions between the groups
# compute the image and kernel of the functions f
# this gives the new homology groups
cone <- function(homA, homB , f){
	H <- as.list(1:(2*length(homA)+2))
	j <- 1
	i <- 1
	for(i in 1:length(homA)){

	  ## Step1: Obtain all the spaces in play
		space1 <- homA[[i]]$subspace
		space2 <- homB[[i]]$subspace
		cbs1 <- homA[[i]]$space
		cbs2 <- homB[[i]]$space
		rel1 <- homA[[i]]$relation_space
    rel2 <- homB[[i]]$relation_space
    spcs1 <- homA[[i]]$space_indices
    spcs2 <- homB[[i]]$space_indices

		degree <- homA[[i]]$degree

		## Obtain the map
		f_curr <- f[[i]]

		## Calculate image and kernel on the whole spaces
		if(all(space1 == 0)) ker_curr <- space1
		else ker_curr <- calculate_intersection_basis(space1,NullSpace(f_curr))

		im_curr <- f_curr%*%space1

		## Calculate representors, mod out the relations
		ker_curr <- apply(ker_curr,
		                  2,
		                  give_representor,
		                  cbs1,
		                  rel1)

		im_curr <-  apply(im_curr,
		                  2,
		                  give_representor,
		                  cbs2,
		                  rel2)


		H[[j]] <- new_homology(ker_curr,complete_basis(ker_curr),rel1,spcs1,degree -1)
		j  <- j+1


		H[[j]] <- new_homology(im_curr,complete_basis(im_curr),rel2,spcs2,degree)
		j <- j+1
	}
	return(H)
}


#### Prep for cone calculation
homA <- baseline_homology(diagram,c(0,0))
homB <- baseline_homology(diagram,c(1,0))
f <- get_cone_map(diagram,level=2, index_vec1 = c(0,0), index_vec2 = c(1,0))
H <- cone(homA,homB,f)
H0 <- clean_up_H(H)

lapply(H,function(x) print(x$degree))

homA <- baseline_homology(diagram,c(0,1))
homB <- baseline_homology(diagram,c(1,1))
f <- get_cone_map(diagram,level=2, index_vec1 = c(0,1), index_vec2 = c(1,1))
H <- cone(homA,homB,f)
H1 <- clean_up_H(H)

g <- get_cone_map(diagram,level=1, index_vec1 = c(0), index_vec2 = c(1))
f <- g
homA <- H0
homB <- H1
H_try <- cone(H0,H1,g)
H_try <- clean_up_H(H_try)





cone_depr <- function(homA, homB , f){
  H <- as.list(1:(2*length(homA)+2))
  j <- 1
  i <- 1
  for(i in 1:length(homA)){

    ## Step1: Obtain all the spaces in play
    space1 <- homA[[i]]$basis
    space2 <- homB[[i]]$basis
    cbs1 <- homA[[i]]$cbs
    cbs2 <- homB[[i]]$cbs
    rel1 <- homA[[i]]$rel
    rel2 <- homB[[i]]$rel

    degree <- homA[[i]]$degree

    ## Obtain the map
    f_curr <- f[[i]]

    ## Calculate image and
    ker_curr <- calculate_intersection_basis(space1,NullSpace(f_curr))
    im_curr <- f_curr%*%space1

    ## Calculate representors
    ker_curr <- apply(ker_curr,
                      2,
                      give_representor,
                      basis1,
                      1:ncol(space1))

    if(all(space2 == 0) && all(space1 == 0)) ker_curr <- 0
    else if (all(space2 == 0) && any(space1 != 0)) ker_curr <- space1
    else if (any(space2 != 0) && all(space1 == 0)) ker_curr <- 0
    else{
      ker_curr <- NullSpace(f_curr)
      if(all(ker_curr == 0)) ker_curr <- 0
    }
    if(any(ker_curr != 0)){
      ker_curr <- apply(ker_curr,
                        2,
                        give_representor,
                        basis1,
                        1:ncol(space1))
      bs1 <- complete_basis(ker_curr)
    } else bs1 <- diag(1,nrow = ncol(f_curr))
    # if(all(space2 == 0)) {
    #   if(all(space1 == 0)) space1 <- matrix(0,ncol=ncol(f_curr),nrow=ncol(f_curr))
    #   ker_curr <- space1
    # } else if(all(space1 == 0)){
    # 	space1 <- matrix(0,ncol=ncol(f_curr),nrow=ncol(f_curr))
    # 	ker_curr <- space1
    # } else{
    # 	ker_curr <- calculate_intersection_basis(space1,NullSpace(f_curr))
    # }
    # 		f_curr%*%space1
    #     space2
    h <- structure(list(degree = degree - 1,
                        basis = ker_curr,
                        basis_changer = bs1),class = "homology")

    H[[j]] <- h
    j  <- j+1



    # f <- basis_changer_2%*%f_curr%*%solve(basis_changer_1)
    # g <- f_curr%*%basis_changer_1[,1:2]
    # space1
    #
    #
    # sapply(1:2,function(x) is_inside(basis_changer_2[,1:6],g[,x]))
    # vec <- solve(basis_changer_2,g[,1])
    # vec[5:8] <- 0
    # vec <- basis_changer_2%*%vec


    #
    # R(cbind(basis_changer_2[,1:4],f[,2]))
    # R(cbind(f[,4],basis_changer_2[,5:8]))
    # R(basis_changer_2)
    #
    # R(basis_changer_2)
    # R(basis_changer_1[,3:4])
    #
    # round(calculate_intersection_basis(f_curr%*%(basis_changer_1[,1:2]),basis_changer_2[,1:4]))
    # calculate_intersection_basis(basis_changer_2%*%f_curr%*%solve(basis_changer_1)[,1:2],space2)
    #
    # calculate_intersection_basis(f,basis_changer_2[,1:4])


    if(all(space1 == 0)) im_curr <- 0
    else im_curr <- f_curr%*%space1
    if(any(im_curr != 0)) {
      im_curr <- apply(im_curr,
                       2,
                       give_representor,
                       basis2,
                       (ncol(space2)+1):ncol(basis2) )

      if(all(im_curr == 0)) im_curr <- 0

    } else im_curr <- 0
    if(any(im_curr != 0)) bs2 <- complete_basis(im_curr)
    else bs2 <- diag(1,nrow = nrow(f_curr))

    h <- structure(list(degree = degree,
                        basis= im_curr,
                        basis_changer = bs2) ,class = "homology")

    H[[j]] <- h
    j  <- j+1
  }
  return(H)
}


