#Smoothes a knot diagram according to the rules specified by corner.
#The output is a list of the tuples that represents the smoothed diagram.
smooth <- function(tf,s){
  tf$s <- s
  tf %>% filter(s==1) %>% select(x,w,y,z) %>% rename(y=w,z=y,w=z)-> tf2
  tf %>% filter(s==0) %>% select(x,y,z,w) -> tf1
  tf <- rbind(tf1,tf2)
  return(tf)
}

get_diagram <- function(ls){
  m <- matrix(0,nrow=1,ncol=4)
  for (i in 1:length(ls)) m <- rbind(m,ls[[i]])
  m <- m[-1,]
  tb <- tibble(x=m[,1],y=m[,2],z=m[,3],w=m[,4])
  return(tb)
}

generate_cycle <- function(index,env){
  #start is the starting point of a cycle
  #index is the index where the cycle is currently
  cycle <- env$r[[index]]
  i <- 1
  while(i <= length(env$r)){
    tuple <- env$r[[i]]
    chain <- intersect(cycle,tuple)
    if(length(chain)>0){
      cycle <- c(cycle,tuple)
      cycle <- unique(cycle)
      env$r[[i]] <- c(0,0)
      i <- 1
    }
    else i <- i+1
  }
  # print(cycle)
  env$cycle <- cycle
  res <- which(unlist(env$r) > 0)
  if(length(res)>0) return(ceiling(min(res)/2))
  else return(0)
}

#Input: A smoothed diagram
#Output: The cycle indices
get_cycles <- function(tf,s){
  tf2 <- smooth(tf,s)
  p <- mapply(`c`,tf2$x,tf2$y,SIMPLIFY = F)
  q <- mapply(`c`,tf2$z,tf2$w,SIMPLIFY = F)
  r <- c(p,q)
  env <- rlang::current_env()
  cycle_indices <- 1
  start <- 1
  cycle <- NULL
  cycles <- list()
  i <- 1
  while(TRUE){
    start <- generate_cycle(start,env)
    cycles[[i]] <- cycle
    i <- i+1
    #cycle_indices <- c(cycle_indices,start)
    if(start == 0) break
  }

  cycle_indices <- sapply(cycles, min)

  #cycle_indices <- cycle_indices[cycle_indices>0]

  space = list(s=s,space=cycle_indices,cycles = cycles)
  return(space)
}

extract_cycles <- function(tb,x,diagram){
  # browser()
  tb %>% select(-sum) -> tb
  tb <- unname(unlist(unclass(tb)))
  get_cycles(diagram,tb)
}



#Generate a Basis for a tensor product of spaces.
get_basis <- function(space){
  y <- length(space)
  z <- rep(2,y)
  w <- 2^y
  ls <- as.list(rep(0,w))
  for(i in 1:w){
    x <- numeric(w)
    x[i] <- 1
    arr <- array(x,dim=z)
    ls[[i]] <- arr
  }
  return(ls)
}

get_crossing_signs <- function(diagram){
  diagram %>%
    filter(y-w == 1 | w-y > 1) %>%
    count -> np
  diagram %>%
    filter(w-y == 1 | y-w > 1) %>%
    count -> nm
  np <- unname(unlist(unclass(np)))
  nm <- unname(unlist(unclass(nm)))
  return(list(np=np,nm=nm))
}


# library(tibble)
# library(rlang)
# library(dplyr)
# library(stringr)
# library(matlib)
# library(pracma)
# library(rvest)
x <- c(1,5,3)
y <- c(5,3,1)
z <- c(2,6,4)
w <- c(4,2,6)

#' Right trefoil knot
#'
#' @export
tf <- tibble(x=x,y=y,z=z,w=w)

#' Hopf link
#'
#' @export
hl <- tibble(x=c(4,2),y=c(1,3),z=c(3,1),w=c(2,4))

#' Left trefoil knot
#'
#' @export
ltf <- tibble(x=c(1,3,5),y=c(4,6,2),z=c(2,4,6),w=c(5,1,3))


#' Knot 5_1
#'
#' @export
knot_5_1 <- get_diagram(
  list(c(1,6,2,7),c(3,8,4,9),c(5,10,6,1),c(7,2,8,3),c(9,4,10,5))
)

#' Knot 10_132
#'
#' @export
knot_10_132 <- get_diagram(
  list(
    c(4,2,5,1),c(8,4,9,3),c(5,12,6,13),c(15,18,16,19),c(9,16,10,17),
    c(17,10,18,11),c(13,20,14,1),c(19,14,20,15),c(11,6,12,7),c(2,8,3,7)
  )
)

knot_4_1 <- get_diagram(
  list(c(4, 2, 5, 1), c(8, 6, 1, 5), c(6, 3, 7, 4), c(2, 7, 3, 8))
)

#' Salomon link
#'
#' @export
salomon <- get_diagram(list(
    c(6,1,7,2),c(8,3,5,4),c(2,5,3,6),c(4,7,1,8)
  )
)

#' Whitehead link
#'
#' @export
whitehead <- get_diagram(list(
  c(6,1,7,2),c(10,7,5,8),c(4,5,1,6),c(2,10,3,9),c(8,4,9,3)
))

# str <- "X[1, 10, 2, 11], X[9, 2, 10, 3], X[3, 7, 4, 6], X[15, 5, 16, 4], X[5,17, 6, 16], X[7, 14, 8, 15], X[8, 18, 9, 17], X[11, 18, 12, 19],X[19, 12, 20, 13], X[13, 20, 14, 1]"
# str <- str_replace_all(str,"X\\[","c\\(")
# str <- str_replace_all(str,"\\]","\\)")
# str <- paste("list(",str,")",sep="")
# ls <- eval(parse_expr(str))
# Millett <- get_diagram(ls)
# rm(str,x,y,z,ls)

