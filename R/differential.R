#Calculate a direct sum of linear maps

get_j_delta <- function(space1,space2,i) {
  for(k in space2$cycles){
    if (length(intersect(k,i)) > 0) break
  }
  for(l in space1$cycles){
    if (length(intersect(l,k)>0)) break
  }
  return(min(l))
}

get_j_mult <- function(space1,space2,i){
  for(k in space1$cycles){
    if (length(intersect(k,i)) > 0) break
  }
  for(l in space2$cycles){
    if (length(intersect(l,k)>0)) break
  }
  return(min(l))
}

# s1 <- l1[[1]]
# s2 <- l2[[3]]
get_map <- function(s1,s2){
  if(sum(s1$s-s2$s != 0) > 1) {
    m <- matrix(0, ncol = 2^length(s1$space), nrow = 2^length(s2$space))
    return(list(m=m,s1=s1$space,s2=s2$space))
  }
  if (length(s1$space) < length(s2$space)){
    i <- setdiff(s2$space,s1$space)
    j <- get_j_delta(s1,s2,i)
    #cat(i,j,"\n")
    k <- min(i,j)
    d <- max(i,j)
    return(delta_map(s1$space,s2$space,k,d))
  }
  else {
    i <- setdiff(s1$space,s2$space)
    j <- get_j_mult(s1,s2,i)
    #cat(i,j,"\n")
    return(mult_map(s1$space,s2$space,i,j))
  }
}


get_index_mat <- function(i,j,dim1,dim2,l1,l2){
  m <- matrix(0, ncol = length(l1), nrow = length(l2))
  dims1 <- lapply(l1,function(x) 2^length(x$space))
  dims2 <- lapply(l2,function(x) 2^length(x$space))
  #i=Index of space in head space
  #j=Index of space in tail space
  m[j,i] <- 1
  m <- m[,rep(1:length(l1),dims1 ),drop=F]
  m <- m[rep(1:length(l2),dims2 ),,drop=F]
  return(m)
}

adjust_signs <- function(d,l1,l2){
  for(i in 1:length(d)){
    for(j in 1:length(d[[i]])){
      q <- which(l1[[i]]$s - l2[[j]]$s != 0)
      if (length(q) > 1) next
      else{
        xi <- l1[[i]]$s[ -(q:length(l1[[i]]$s)) ]
        sign <- (-1)^sum(xi)
        d[[i]][[j]]$m <- sign*d[[i]][[j]]$m
      }
    }
  }
  return(d)
}

#Given a list of two linear spaces give the corresponding complex map
#Getting signs right will be something else
#The maps will be mapping l1 --> l2
differential <- function(l1,l2) {

  #Get Edge maps
  #Set up a list that is long enough
  d <- as.list(1:length(l1))
  for (j in 1:length(l1)) d[[j]] <- as.list(1:length(l2))

  for (i in 1:length(l1)){
    for (j in 1:length(l2)) {
      d[[i]][[j]] <- get_map(l1[[i]],l2[[j]])
    }
  }

  #Adjust the signs of the differentials
  d <- adjust_signs(d,l1,l2)

  #Dimension of the tail space
  dim1 <- 0
  for(i in l1){
    dim1 <- dim1 + 2^length(i$space)
  }

  #Dimension of the head space
  dim2 <- 0
  for(i in l2){
    dim2 <- dim2 + 2^length(i$space)
  }

  # #Write the maps into a matrix
  # a <- matrix(0,ncol = dim1, nrow = dim2)
  # for(i in 1:length(l1)){
  #   # cat("i=",i,"\t")
  #   for(j in 1:length(l2)){
  #     # cat("j=",j," ")
  #     b <- get_index_mat(i,j,dim1,dim2,l1,l2)
  #     a[b==1] <- d[[i]][[j]]$m
  #   }
  #   # cat("\n")
  # }

  m <- matrix(0,nrow=dim2,ncol=1)
  for(j in 1:length(l1)){
    # cat("j=",j,"\t")
    a <- d[[j]][[1]]$m
    if(length(d[[j]]) > 1){
      for(i in 2:length(d[[j]])){
        # cat("i=",i," ")
        a <- rbind(a,d[[j]][[i]]$m)
      }
    }
    m <- cbind(m,a)
    # cat("\n")
  }


  spaces1 <- lapply(l1,function(x) x$space)
  spaces2 <- lapply(l2,function(x) x$space)

  return(list(m=m[,-1],spaces1=spaces1,spaces2=spaces2))
}


get_differential <- function(diagram){
  cat("################ Calculating differentials of complex.\n")
  crossings <- nrow(diagram)
  ls <- rep(as.list(0),crossings)
  ls <- lapply(ls, function(x) x <- c(0,1))
  grid <- expand.grid(ls)
  grid <- as_tibble(grid)
  grid %>% mutate(sum = rowSums(grid)) %>%
    arrange(sum) %>% rowwise() -> grid2

  ls <- group_map(grid2,extract_cycles,diagram)
  grid2$list <- ls

  grid2 %>% ungroup() %>%
    group_by(sum) -> grid3

  grid4 <- group_map(grid3,get_input_lists)

  # grid5 <- list(numeric(crossings))
  # for(i in 1:crossings) grid5[[i]] <- grid4[[i+1]]
  # differential(grid4[[3]],grid4[[4]])

  d <- list(1:crossings)
  for(i in 1:crossings){
    cat("Computed",i,"th map.\n")
    d[[i]] <- differential(grid4[[i]],grid4[[i+1]])
  }
  return(d)
}


